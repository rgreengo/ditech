<?php
class UsuariosModel extends TRecord
{
    const TABLENAME = 'tbl_usuarios';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
    
    /**
     * Constructor method
     * @param $id Primary key to be loaded (optional)
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('nome');
        parent::addAttribute('senha');        
        parent::addAttribute('email');
        parent::addAttribute('cpf');
        parent::addAttribute('ativo');
    }
}