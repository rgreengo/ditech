-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Jun-2019 às 19:21
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ditech-db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_autoridade`
--

CREATE TABLE `tbl_autoridade` (
  `id` int(11) NOT NULL,
  `autoridade` varchar(150) NOT NULL,
  `ativo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_autoridade`
--

INSERT INTO `tbl_autoridade` (`id`, `autoridade`, `ativo`) VALUES
(1, 'admin', 1),
(2, 'publico', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_locacoes`
--

CREATE TABLE `tbl_locacoes` (
  `id` int(11) NOT NULL,
  `sala_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `hora_locacao` int(11) NOT NULL,
  `dia_locacao` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_opcionais`
--

CREATE TABLE `tbl_opcionais` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `ativo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_opcionais`
--

INSERT INTO `tbl_opcionais` (`id`, `nome`, `ativo`) VALUES
(1, 'ar condicionado', 1),
(2, 'microfone', 1),
(3, 'televisão', 1),
(4, 'banheiro', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_reservas`
--

CREATE TABLE `tbl_reservas` (
  `id` int(11) NOT NULL,
  `sala_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `data_reserva` date NOT NULL,
  `hora_reserva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_reservas`
--

INSERT INTO `tbl_reservas` (`id`, `sala_id`, `usuario_id`, `data_reserva`, `hora_reserva`) VALUES
(1, 4, 2, '2019-06-13', 9),
(2, 3, 2, '2019-05-23', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_salas`
--

CREATE TABLE `tbl_salas` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_fim` date NOT NULL,
  `descricao` text NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `tipo_sala_id` int(11) NOT NULL,
  `tipo_horario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_salas`
--

INSERT INTO `tbl_salas` (`id`, `nome`, `data_inicio`, `data_fim`, `descricao`, `ativo`, `tipo_sala_id`, `tipo_horario_id`) VALUES
(1, 'sala de reunião irmãos josé', '2015-11-25', '2019-06-18', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet facilisis sapien. Phasellus aliquam feugiat eleifend. Quisque laoreet ornare vulputate. Donec luctus sodales aliquam. Donec laoreet mi in molestie gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vehicula fringilla pretium.\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus placerat lacus non nunc iaculis, sed gravida ante hendrerit. Fusce ut cursus neque. Suspendisse finibus eros non dolor fringilla, sed imperdiet justo auctor. Nulla dolor lectus, vehicula sit amet blandit vel, posuere eu leo. Sed vitae lorem id mi dapibus maximus. Nunc egestas porta mauris et convallis. Donec eu efficitur libero.', 1, 3, 1),
(3, 'sala 03', '2019-05-05', '2019-06-21', 'desc', 1, 3, 2),
(4, 'sala 04', '2019-06-04', '2019-06-28', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet facilisis sapien. Phasellus aliquam feugiat eleifend. Quisque laoreet ornare vulputate. Donec luctus sodales aliquam. Donec laoreet mi in molestie gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vehicula fringilla pretium.\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus placerat lacus non nunc iaculis, sed gravida ante hendrerit. Fusce ut cursus neque. Suspendisse finibus eros non dolor fringilla, sed imperdiet justo auctor. Nulla dolor lectus, vehicula sit amet blandit vel, posuere eu leo. Sed vitae lorem id mi dapibus maximus. Nunc egestas porta mauris et convallis. Donec eu efficitur libero.', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_salas_has_opcionais`
--

CREATE TABLE `tbl_salas_has_opcionais` (
  `id` int(11) NOT NULL,
  `sala_id` int(11) NOT NULL,
  `opcionais_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_salas_has_opcionais`
--

INSERT INTO `tbl_salas_has_opcionais` (`id`, `sala_id`, `opcionais_id`) VALUES
(1, 1, 4),
(2, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_horario`
--

CREATE TABLE `tbl_tipo_horario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `hora_inicio` int(20) NOT NULL,
  `hora_fim` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_tipo_horario`
--

INSERT INTO `tbl_tipo_horario` (`id`, `tipo`, `hora_inicio`, `hora_fim`) VALUES
(1, 'comercial', 8, 18),
(2, 'full time', 6, 24),
(3, 'noturno', 14, 22);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_sala`
--

CREATE TABLE `tbl_tipo_sala` (
  `id` int(11) NOT NULL,
  `tamanho` varchar(10) NOT NULL,
  `num_pessoa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_tipo_sala`
--

INSERT INTO `tbl_tipo_sala` (`id`, `tamanho`, `num_pessoa`) VALUES
(1, 'Grande', 500),
(2, 'Medio', 280),
(3, 'GG', 1500);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `autotidade_id` int(11) NOT NULL,
  `ativo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id`, `nome`, `senha`, `email`, `cpf`, `autotidade_id`, `ativo`) VALUES
(1, 'RAFAEL BORTOLINI', '123123', 'rgreengo@gmail.com', '004.324.200-61', 1, 0),
(2, 'jose silva 2', '123123', 'josesilva@gmail.com', '004.324.200-61', 1, 1),
(4, 'pedro silva 2', '123', 'pedrosilva@gmail.com', '004.324.200-61', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_autoridade`
--
ALTER TABLE `tbl_autoridade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_locacoes`
--
ALTER TABLE `tbl_locacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sala_id` (`sala_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indexes for table `tbl_opcionais`
--
ALTER TABLE `tbl_opcionais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_reservas`
--
ALTER TABLE `tbl_reservas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sala_id` (`sala_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indexes for table `tbl_salas`
--
ALTER TABLE `tbl_salas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_sala_id` (`tipo_sala_id`),
  ADD KEY `tipo_horario_id` (`tipo_horario_id`);

--
-- Indexes for table `tbl_salas_has_opcionais`
--
ALTER TABLE `tbl_salas_has_opcionais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sala_id` (`sala_id`),
  ADD KEY `opcionais_id` (`opcionais_id`);

--
-- Indexes for table `tbl_tipo_horario`
--
ALTER TABLE `tbl_tipo_horario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tipo_sala`
--
ALTER TABLE `tbl_tipo_sala`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_EMAIL` (`email`),
  ADD KEY `autotidade_id` (`autotidade_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_autoridade`
--
ALTER TABLE `tbl_autoridade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_locacoes`
--
ALTER TABLE `tbl_locacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_opcionais`
--
ALTER TABLE `tbl_opcionais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_reservas`
--
ALTER TABLE `tbl_reservas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_salas`
--
ALTER TABLE `tbl_salas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_salas_has_opcionais`
--
ALTER TABLE `tbl_salas_has_opcionais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_tipo_horario`
--
ALTER TABLE `tbl_tipo_horario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_tipo_sala`
--
ALTER TABLE `tbl_tipo_sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbl_locacoes`
--
ALTER TABLE `tbl_locacoes`
  ADD CONSTRAINT `tbl_locacoes_ibfk_1` FOREIGN KEY (`sala_id`) REFERENCES `tbl_salas` (`id`),
  ADD CONSTRAINT `tbl_locacoes_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `tbl_usuarios` (`id`);

--
-- Limitadores para a tabela `tbl_reservas`
--
ALTER TABLE `tbl_reservas`
  ADD CONSTRAINT `tbl_reservas_ibfk_1` FOREIGN KEY (`sala_id`) REFERENCES `tbl_salas` (`id`),
  ADD CONSTRAINT `tbl_reservas_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `tbl_usuarios` (`id`);

--
-- Limitadores para a tabela `tbl_salas`
--
ALTER TABLE `tbl_salas`
  ADD CONSTRAINT `tbl_salas_ibfk_1` FOREIGN KEY (`tipo_sala_id`) REFERENCES `tbl_tipo_sala` (`id`),
  ADD CONSTRAINT `tbl_salas_ibfk_2` FOREIGN KEY (`tipo_sala_id`) REFERENCES `tbl_tipo_sala` (`id`),
  ADD CONSTRAINT `tbl_salas_ibfk_3` FOREIGN KEY (`tipo_horario_id`) REFERENCES `tbl_tipo_horario` (`id`);

--
-- Limitadores para a tabela `tbl_salas_has_opcionais`
--
ALTER TABLE `tbl_salas_has_opcionais`
  ADD CONSTRAINT `tbl_salas_has_opcionais_ibfk_1` FOREIGN KEY (`sala_id`) REFERENCES `tbl_salas` (`id`),
  ADD CONSTRAINT `tbl_salas_has_opcionais_ibfk_2` FOREIGN KEY (`opcionais_id`) REFERENCES `tbl_opcionais` (`id`);

--
-- Limitadores para a tabela `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD CONSTRAINT `tbl_usuarios_ibfk_1` FOREIGN KEY (`autotidade_id`) REFERENCES `tbl_autoridade` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
