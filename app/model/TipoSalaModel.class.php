<?php
class TipoSalaModel extends TRecord
{
    const TABLENAME = 'tbl_tipo_sala';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
    
    /**
     * Constructor method
     * @param $id Primary key to be loaded (optional)
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('tamanho');
        parent::addAttribute('num_pessoa');                
    }
}