
select distinct t.nome_completo , t.departamento , t.dias_trabalhados
from(
select dept.dept_name as departamento , concat(emp.first_name , ' ' , emp.last_name) as nome_completo , DATEDIFF(dep.to_date , dep.from_date) as dias_trabalhados from employees emp 
join dept_emp dep on emp.emp_no = dep.emp_no
join titles tit on emp.emp_no = tit.emp_no
join salaries salary on emp.emp_no = salary.emp_no 
join departments dept on dep.dept_no = dept.dept_no 
)t
order by dias_trabalhados desc
LIMIT 10