<?php
class SalasModel extends TRecord
{
    const TABLENAME = 'tbl_salas';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
    
    /**
     * Constructor method
     * @param $id Primary key to be loaded (optional)
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('nome');
        parent::addAttribute('data_inicio');        
        parent::addAttribute('data_fim');        

        parent::addAttribute('descricao');
        parent::addAttribute('ativo');
        parent::addAttribute('tipo_sala_id');
        parent::addAttribute('tipo_horario_id');
    }
}