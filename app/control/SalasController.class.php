<?php
use Adianti\Widget\Form\TPassword;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Widget\Dialog\TMessage;
use Dompdf\Exception;
use Adianti\Widget\Form\TDate;
use Adianti\Widget\Form\TText;
use Adianti\Widget\Wrapper\TDBCombo;

/**
 *
 * @author  <your name here>
 */
class SalasController extends TStandardForm
{
    protected $form;      // form
    protected $datagrid;  // datagrid
    protected $loaded;
    protected $pageNavigation;  // pagination component
    
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct($param)
    {
        parent::__construct();
        parent::setDatabase('ditech-db');
        parent::setActiveRecord('SalasModel');


        $this->datagrid = new TDataGrid;
        
        // create the datagrid columns
        $idCol         = new TDataGridColumn('id', 'id', 'center', '10%');
        $diCol         = new TDataGridColumn('data_inicio', 'Data Início', 'left', '30%');
        $dfCol         = new TDataGridColumn('data_fim', 'Data Fim', 'left', '30%');
        $nomeCol       = new TDataGridColumn('nome', 'Nome', 'left', '30%');
        $ativoCol      = new TDataGridColumn('ativo', 'Ativo', 'left', '30%');
        
        // add the columns to the datagrid
        $this->datagrid->addColumn($idCol);
        $this->datagrid->addColumn($nomeCol);
        $this->datagrid->addColumn($diCol);
        $this->datagrid->addColumn($dfCol);
        $this->datagrid->addColumn($ativoCol);


        // creates two datagrid actions
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('fa:edit blue');
        $action1->setFields(['id', 'nome']);
          
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('fa:trash red');
        $action2->setField('id');
          
        // add the actions to the datagrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        $this->datagrid->width = '100%';
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_salas');
        $this->form->setFormTitle('Gerenciamento de Salas');
        $id   = new TEntry('id');
        $nome = new TEntry('nome');
        $data_inicio = new TDate('data_inicio');
        $data_inicio->setMask('dd/mm/yyyy');    

        $data_fim   = new TDate('data_fim');
        $data_fim->setMask('dd/mm/yyyy');    

        $descricao   = new TText('descricao');
        $ativo   = new TCombo('ativo');
        
        $ativo_itens = array();
        $ativo_itens[0] = 'desativado';
        $ativo_itens[1] = 'ativo';
        
        $ativo->addItems($ativo_itens);

        $tipo_sala_id = new TDBCombo('tipo_sala_id' , 'ditech-db' , 'TipoSalaModel' , 'id' , 'tamanho' , 'tamanho');

        $tipo_horario_id = new TDBCombo('tipo_horario_id' , 'ditech-db' , 'TipoHorarioModel' , 'id' , 'tipo' , 'tipo');

        $nome->addValidation('Nome', new TRequiredValidator());
          
        
        
        $id->setEditable(false);
        $id->setSize(100);
        $descricao->setSize('50%');
        $nome->setSize('50%');
        $data_inicio->setSize('50%');
        $data_fim->setSize('50%');
        $ativo->setSize('50%');
        $tipo_sala_id->setSize('50%');
        $tipo_horario_id->setSize('50%');
        
        $this->form->addFields([new TLabel('Id:')], [$id]);
        $this->form->addFields([new TLabel('Nome:', '#ff0000')], [$nome]);
        $this->form->addFields([new TLabel('Data inicio:')], [$data_inicio]);
        $this->form->addFields([new TLabel('Data fim:')], [$data_fim]);
        $this->form->addFields([new TLabel('Ativo:')], [$ativo]);
        $this->form->addFields([new TLabel('Descrição:')], [$descricao]);

        $this->form->addFields([new TLabel('Tipo Sala:')], [$tipo_sala_id]);

        $this->form->addFields([new TLabel('Tipo Horário:')], [$tipo_horario_id]);


        // create the form actions
        $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o')->addStyleClass('btn-primary');
        $this->form->addAction('Limpar formulário', new TAction([$this, 'onClear']), 'fa:eraser #dd5a43');
        

        // creates the datagrid model
        $this->datagrid->createModel();
        
         


        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($this->datagrid);
       
        parent::add($container);
    }


    /**
     * Load the data into the datagrid
     */
    public function onReload($param = null)
    {
        $this->datagrid->clear();

        try {
            TTransaction::open('ditech-db');
            $repository = new TRepository('SalasModel');


            $objects = $repository->load();
            if ($objects) {
                foreach ($objects as $obj) {
                    $obj->data_inicio = TDate::date2br($obj->data_inicio);
                    $obj->data_fim = TDate::date2br($obj->data_fim);

                    $obj->ativo = ($obj->ativo ? "Ativo" : "Desativada");
                    $this->datagrid->addItem($obj);
                }
            }

            TTransaction::close();
            $this->loaded=true;
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
        }
    }

    public function show()
    {
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }



    public function onEdit($param)
    {
         // get the parameter and shows the message
         $id = $param['id'];
         try {
             TTransaction::open('ditech-db');
             $object = new SalasModel($id);
           
             $object->data_inicio = TDate::date2br($object->data_inicio);
             $object->data_fim = TDate::date2br($object->data_fim);

             $this->form->setData($object);
 
             TTransaction::close();            
             
         } catch (Exception $e) {
             new TMessage('erro', $e->getMessage());
             TTransaction::rollback();
         }
    }

    public function onDelete($param)
    {
        // get the parameter and shows the message
        $id = $param['id'];
        try {
            TTransaction::open('ditech-db');
            $object = new SalasModel($id);
            $object->delete();           

            TTransaction::close();            
            new TMessage('info' , 'Registro excluído com sucesso');
            $this->onReload(); 

        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    public function onSave()
    {
        try {
            TTransaction::open('ditech-db');
            $object = $this->form->getData('SalasModel');
          
            $object->data_inicio = TDate::date2us($object->data_inicio);
            $object->data_fim = TDate::date2us($object->data_fim);

            $object->store();

            TTransaction::close();
            new TMessage('info' , 'Registro salvo com sucesso');
            $this->onReload(); 

            
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }
}
