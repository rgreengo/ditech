<?php
use Adianti\Widget\Form\TPassword;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Widget\Dialog\TMessage;
use Dompdf\Exception;
use Adianti\Widget\Form\TDate;
use Adianti\Widget\Form\TText;
use Adianti\Widget\Wrapper\TDBCombo;
use Adianti\Widget\Form\TCombo;
use Adianti\Widget\Form\TLabel;

/**
 *
 * @author  <your name here>
 */
class ReservasController extends TStandardForm
{
    protected $form;      // form
    protected $datagrid;  // datagrid
    protected $loaded;
    protected $pageNavigation;  // pagination component
    
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct($param)
    {
        parent::__construct();
        parent::setDatabase('ditech-db');
        parent::setActiveRecord('ReservasModel');


        $this->datagrid = new TDataGrid;
        
        // create the datagrid columns
        $idCol              = new TDataGridColumn('id', 'id', 'center', '10%');
        $data_reservaCol    = new TDataGridColumn('data_reserva', 'Data Reserva', 'left', '30%');
        $hora_reservaCol    = new TDataGridColumn('hora_reserva', 'Hora Reserva', 'left', '30%');
        $usuario_idCol      = new TDataGridColumn('usuario_id', 'Usuário', 'left', '30%');
        $sala_idCol         = new TDataGridColumn('sala_id', 'Sala', 'left', '30%');
        
        // add the columns to the datagrid
        $this->datagrid->addColumn($idCol);
        $this->datagrid->addColumn($data_reservaCol);
        $this->datagrid->addColumn($hora_reservaCol);
        $this->datagrid->addColumn($usuario_idCol);
        $this->datagrid->addColumn($sala_idCol);


        // creates two datagrid actions
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('fa:edit blue');
        $action1->setFields(['id', 'nome']);
          
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('fa:trash red');
        $action2->setField('id');
          
        // add the actions to the datagrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        $this->datagrid->width = '100%';
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_reservas');
        $this->form->setFormTitle('Gerenciamento de Reservas');
        $id   = new TEntry('id');
        

        $data_reserva   = new TDate('data_reserva');
        $data_reserva->setMask('dd/mm/yyyy');  
        
        
        $hora_reserva = new TCombo('hora_reserva');
        

        $sala_id = new TDBCombo('sala_id' , 'ditech-db' , 'SalasModel' , 'id' , 'nome' , 'nome');
        $exit_action_sala = new Adianti\Control\TAction(array($this, 'getHorarios'));
        $sala_id->setChangeAction($exit_action_sala);     

        $usuario_id = new TDBCombo('usuario_id' , 'ditech-db' , 'UsuariosModel' , 'id' , 'nome' , 'nome');
        $datas_disponiveis = new TLabel('datas_disponiveis');
        
          
        
        
        $id->setEditable(false);
        $id->setSize(100);
        $data_reserva->setSize('50%'); 
        $hora_reserva->setSize('50%');     

        $sala_id->setSize('50%');
        $usuario_id->setSize('50%');
        
        
        $this->form->addFields([new TLabel('Id:')], [$id]);

        $this->form->addFields([new TLabel('Usuário:')], [$usuario_id]); 
        $this->form->addFields([new TLabel('Sala:')], [$sala_id]);

        $this->form->addFields([new TLabel('Data Reserva:')], [$data_reserva]);      
        $this->form->addFields([new TLabel('Hora Reserva:')], [$hora_reserva]);   
                       


        // create the form actions
        $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o')->addStyleClass('btn-primary');
        $this->form->addAction('Limpar formulário', new TAction([$this, 'onClear']), 'fa:eraser #dd5a43');
        

        // creates the datagrid model
        $this->datagrid->createModel();

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($this->datagrid);
       
        parent::add($container);
    }


    public static function getHorarios($param)
    {
        
        try {
            TTransaction::open('ditech-db');
            
            if ($param['sala_id'] > 0) {
                $conn = TTransaction::get(); // obtém a conexão
            
                $result = $conn->query('SELECT * FROM tbl_salas 
                join tbl_tipo_horario on tbl_salas.tipo_horario_id = tbl_tipo_horario.id
                where tbl_salas.id = '.$param['sala_id']);
            
                $horarios_items = array();

                $hora_inicio = 0;
                $hora_fim = 0;

                $data_inicio = "";
                $data_fim = "";

                foreach ($result as $row) {
                    $hora_inicio = $row['hora_inicio'];
                    $hora_fim = $row['hora_fim'];

                    $data_inicio =  TDate::date2br($row['data_inicio']);
                    $data_fim = TDate::date2br($row['data_fim']);
                }

                for ($i=$hora_inicio; $i <= $hora_fim; $i++) {
                    $horarios_items[$i] = $i;
                }

                TTransaction::close();
                $message = "Esta sala está disponivel do dia ".$data_inicio. " até o dia ".$data_fim;

                TCombo::reload('form_reservas', 'hora_reserva', $horarios_items);
                new TMessage('info', $message);
            }
            
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
        }
    }

    /**
     * Load the data into the datagrid
     */
    public function onReload($param = null)
    {
        $this->datagrid->clear();

        try {
            TTransaction::open('ditech-db');
            $repository = new TRepository('ReservasModel');
            $objects = $repository->load();

            if ($objects) {
                foreach ($objects as $obj) {
                    $obj->data_reserva = TDate::date2br($obj->data_reserva);      
                    $obj->hora_reserva .= "Hrs";                   
                   
                    $this->datagrid->addItem($obj);
                }
            }          



            TTransaction::close();
            $this->loaded=true;
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
        }
    }

    public function show()
    {
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }



    public function onEdit($param)
    {
         // get the parameter and shows the message
         $id = $param['id'];
         try {
             TTransaction::open('ditech-db');
             $object = new ReservasModel($id);
           
             $object->data_reserva = TDate::date2br($object->data_reserva); 

             $this->form->setData($object);
             
             
             $array = [
                "sala_id" => $object->sala_id
            ];
 
            $this->getHorarios($array);
             TTransaction::close();            
             
         } catch (Exception $e) {
             new TMessage('erro', $e->getMessage());
             TTransaction::rollback();
         }
    }

    public function onDelete($param)
    {
        // get the parameter and shows the message
        $id = $param['id'];
        try {
            TTransaction::open('ditech-db');
            $object = new ReservasModel($id);
            $object->delete();           

            TTransaction::close();            
            new TMessage('info' , 'Registro excluído com sucesso');
            $this->onReload(); 

        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    public function onSave()
    {
        try {
            TTransaction::open('ditech-db');
            $object = $this->form->getData('ReservasModel');
          
            $object->data_reserva = TDate::date2us($object->data_reserva);
            
            $object->store();

            TTransaction::close();
            new TMessage('info' , 'Registro salvo com sucesso');
            $this->onReload(); 

            
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }
}
