<?php
use Adianti\Widget\Form\TPassword;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Widget\Dialog\TMessage;
use Dompdf\Exception;

/**
 *
 * @author  <your name here>
 */
class UsuariosController extends TStandardForm
{
    protected $form;      // form
    protected $datagrid;  // datagrid
    protected $loaded;
    protected $pageNavigation;  // pagination component
    
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct($param)
    {
        parent::__construct();
        parent::setDatabase('ditech-db');
        parent::setActiveRecord('UsuariosModel');


        $this->datagrid = new TDataGrid;
        
        // create the datagrid columns
        $idCol       = new TDataGridColumn('id', 'id', 'center', '10%');
        $nomeCol     = new TDataGridColumn('nome', 'Nome', 'left', '30%');
        $emailCol    = new TDataGridColumn('email', 'E-mail', 'left', '30%');
        $cpfCol      = new TDataGridColumn('cpf', 'CPF', 'left', '30%');
        
        // add the columns to the datagrid
        $this->datagrid->addColumn($idCol);
        $this->datagrid->addColumn($nomeCol);
        $this->datagrid->addColumn($emailCol);
        $this->datagrid->addColumn($cpfCol);


        // creates two datagrid actions
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('fa:edit blue');
        $action1->setFields(['id', 'nome']);
          
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('fa:trash red');
        $action2->setField('id');
          
        // add the actions to the datagrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        $this->datagrid->width = '100%';
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_usuarios');
        $this->form->setFormTitle('Gerenciamento de usuários');
        $id   = new TEntry('id');
        $nome = new TEntry('nome');
        $senha = new TPassword('senha');
        $email   = new TEntry('email');
        $cpf   = new TEntry('cpf');
        $ativo   = new TEntry('ativo');
        
        
        $nome->addValidation('Nome', new TRequiredValidator());
        $email->addValidation('Senha', new TRequiredValidator());
        $cpf->addValidation('CPF', new TRequiredValidator());
        
        
        $id->setEditable(false);
        $id->setSize(100);
        $email->setSize('50%');
        $nome->setSize('50%');
        $senha->setSize('50%');
        $cpf->setSize('50%');
        $id->setEditable(false);
        
        $this->form->addFields([new TLabel('Id:')], [$id]);
        $this->form->addFields([new TLabel('Nome:', '#ff0000')], [$nome]);
        $this->form->addFields([new TLabel('E-mail:')], [$email]);
        $this->form->addFields([new TLabel('Senha:')], [$senha]);
        $this->form->addFields([new TLabel('CPF:')], [$cpf]);

        // create the form actions
        $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o')->addStyleClass('btn-primary');
        $this->form->addAction('Limpar formulário', new TAction([$this, 'onClear']), 'fa:eraser #dd5a43');
        

        // creates the datagrid model
        $this->datagrid->createModel();
        
         


        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($this->datagrid);
       
        parent::add($container);
    }


    /**
     * Load the data into the datagrid
     */
    public function onReload($param = null)
    {
        $this->datagrid->clear();

        try {
            TTransaction::open('ditech-db');
            $repository = new TRepository('UsuariosModel');


            $objects = $repository->load();
            if ($objects) {
                foreach ($objects as $obj) {
                    $this->datagrid->addItem($obj);
                }
            }

            TTransaction::close();
            $this->loaded=true;
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
        }
    }

    public function show()
    {
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }



    public function onEdit($param)
    {
         // get the parameter and shows the message
         $id = $param['id'];
         try {
             TTransaction::open('ditech-db');
             $object = new UsuariosModel($id);
           
             $this->form->setData($object);
 
             TTransaction::close();            
             
         } catch (Exception $e) {
             new TMessage('erro', $e->getMessage());
             TTransaction::rollback();
         }
    }

    public function onDelete($param)
    {
        // get the parameter and shows the message
        $id = $param['id'];
        try {
            TTransaction::open('ditech-db');
            $object = new UsuariosModel($id);
            $object->delete();           

            TTransaction::close();            
            new TMessage('info' , 'Registro excluído com sucesso');
            $this->onReload(); 

        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    public function onSave()
    {
        try {
            TTransaction::open('ditech-db');
            $object = $this->form->getData('UsuariosModel');
          
            $object->store();

            TTransaction::close();
            new TMessage('info' , 'Registro salvo com sucesso');
            $this->onReload(); 

            
        } catch (Exception $e) {
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }
}
