<?php
class TipoHorarioModel extends TRecord
{
    const TABLENAME = 'tbl_tipo_horario';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
    
    /**
     * Constructor method
     * @param $id Primary key to be loaded (optional)
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('tipo');
        parent::addAttribute('hora_inicio');        
        parent::addAttribute('hora_fim');          
    }
}