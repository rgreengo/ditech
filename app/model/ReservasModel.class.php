<?php
class ReservasModel extends TRecord
{
    const TABLENAME = 'tbl_reservas';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
    
    /**
     * Constructor method
     * @param $id Primary key to be loaded (optional)
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('sala_id');
        parent::addAttribute('usuario_id');

        parent::addAttribute('data_reserva');        
        parent::addAttribute('hora_reserva');           
    }

    public function getSala()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('id', '=', $this->sala_id));
        return SalasModel::getObjects( $criteria );
    }
}